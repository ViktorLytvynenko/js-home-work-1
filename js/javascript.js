/// Teory

// 1. Як можна оголосити змінну у Javascript? -
// Через let или const. Ранее через var;
// 2. У чому різниця між функцією prompt та функцією confirm? -
// prompt дает возможность ввести или не вводить какое-то значение с кнопками ОК или отмена, а confirm только дает возможность нажать на кнопку ОК или отмена.
// 3. Що таке неявне перетворення типів? Наведіть один приклад. -
// 25 / "5". Здесь мы делим число на строку. Результат все равно выведится. Отчасти оно помогает уменьшить код, но может создать проблемы.


/// Practice

// Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name.
// Скопіюйте це значення в змінну admin і виведіть його в консоль.
//
// Оголосити змінну days і ініціалізувати її числом від 1 до 10.
// Перетворіть це число на кількість секунд і виведіть на консоль.
// Запитайте у користувача якесь значення і виведіть його в консоль.

// 1

let admin;
let name;

name = "Viktor";
admin = name;

result1 = document.getElementById("myName")
result1.innerText = admin

// to console

console.log(admin)

// 2

let days = 3
let seconds = days * 24 * 60 * 60

result2 = document.getElementById("days")
result2.innerText = seconds

// to console

console.log(seconds)

//3

question = prompt("How you doing?", "")
result3 = document.getElementById("questionResult")
result3.innerText = question

// to console

console.log(question)
